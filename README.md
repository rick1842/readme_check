> Docker PHP development stack: PHP-FPM, MySQL, Redis, MongoDB and RabbitMQ.

## What's inside

* [PHP-FPM (7.3)](http://php-fpm.org/)
* [MySQL (8.0)](http://www.mysql.com/)

## Requirements

* [Docker Desktop for Windows](https://docs.docker.com/installation/)

## Running

* Start a Docker Desktop and then run:

```sh
$ docker-compose up -d
```




* Get <app_container_id> by run command below:
```sh
$ docker ps
```
> <app_container_id> is CONTAINER_ID of nightly-backend-api_app (IMAGE).

* Run docker exec on an app container
```sh
$ docker exec -it <app_container_id> sh
```


* Install vendors:

```sh
$ composer install
```

* Generate key pair for JWT authentication

```sh
$ openssl genrsa -out ./var/jwt/private.pem -aes256 4096

$ openssl rsa -pubout -in ./var/jwt/private.pem -out ./var/jwt/public.pem
```
> Should use __urbanos__ for keypair passphrase

* (OPTIONAL) Or run docker with specific services:

```sh
$ docker-compose up app mysql
```

* [Localhost](http://localhost:8000/)

## Setting up .env and database

* Open .env file and edit directory of JWT secret and public key was generated before.

Example:

```
JWT_SECRET_KEY=./var/jwt/private.pem
JWT_PUBLIC_KEY=./var/jwt/public.pem
JWT_PASSPHRASE=urbanos
```

* Prepare database

```sh
$ php bin/console doctrine:database:drop --if-exists --force
$ php bin/console doctrine:database:create
$ php bin/console doctrine:schema:update --force --dump-sql
$ php bin/console doctrine:fixtures:load --no-interaction
```

## Testing

* Check coding standard:

```sh 
$ php ./vendor/bin/php-cs-fixer fix --diff --dry-run -v
```
> You can remove _--dry-run_ option to automatically standardize the code

* Unit & Functional Test:

```sh
$ php ./bin/phpunit
```

* Test whole project:

```sh
$ composer test
```

* Fix timeout

```sh
$ export COMPOSER_PROCESS_TIMEOUT=600
```